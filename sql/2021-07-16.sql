ALTER TABLE spider_config ADD COLUMN user_define_pipeline VARCHAR(100) DEFAULT NULL COMMENT '自定义设置';

DROP TABLE IF EXISTS `sys_bug`;
CREATE TABLE `sys_bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户',
  `bug_place` varchar(20) DEFAULT NULL COMMENT 'bug出处',
  `version` varchar(20) DEFAULT NULL COMMENT '版本号',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `imgs` varchar(255) DEFAULT NULL COMMENT '截图',
  `detail` text COMMENT '详情',
  `audit_state` smallint(6) DEFAULT '0' COMMENT '审核状态',
  `score` int(11) DEFAULT '0' COMMENT 'bug审核通过给与的积分奖励',
  `audit_desc` varchar(255) DEFAULT NULL COMMENT '审核说明',
  `audit_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '审核时间',
  `audit_user_id` varchar(20) DEFAULT NULL COMMENT '审核人ID',
  `audit_user_name` varchar(255) DEFAULT NULL COMMENT '审核人',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '反馈时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_donate`;
CREATE TABLE `cms_donate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `platform` varchar(20) DEFAULT NULL COMMENT '平台',
  `uid` varchar(20) DEFAULT NULL COMMENT '平台用户ID',
  `avatar` text COMMENT '头像',
  `amount` decimal(10,2) unsigned DEFAULT NULL COMMENT '金额',
  `msg` varchar(255) DEFAULT NULL COMMENT '留言',
  `donate_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '捐赠时间',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
